import os
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from json import dumps

class EsDb:
    es_connection: None

    def __init__(self):
        self.es_connection=Elasticsearch([os.getenv('ES_URL')])

    def get_result(self,word):

        query = {
            "size": 1000,
            "query": {
                "match": {
                    "body": '"' + word + '"'
                }
            }
        }
        result=""
        for item in helpers.scan(self.es_connection, index="news", doc_type="article",
                                 preserve_order=False, query=query):
            result += dumps(item["_source"]) + os.linesep

        return result
