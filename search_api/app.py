import json

from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop

from es_db import EsDb


class SearchApiHandler(RequestHandler):
    def get(self):
        self.write("SearchApi Server is running..")


class FetchHandler(RequestHandler):
    def post(self):
        self.write(self.settings['es_db'].get_result(word=json.loads(self.request.body).get('word')))

def make_app():
    urls = [
        ("/", SearchApiHandler),
        ("/v1/fetch", FetchHandler)
    ]
    return Application(urls, debug=True,
                       es_db=EsDb())

if __name__ == '__main__':
    app = make_app()
    app.listen(80)
    IOLoop.instance().start()
