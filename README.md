# Coding challenge
This page consists of a coding challenge for Data Engineering roles.
You should be able to complete this in 5 hours, though overall deadline is 5 days to buffer life's other commitments :)

# Goals
There are 3 aims for this test

- Learn how to think from a design perspective
- Assess your development ability i.e. code
- Understand your depth of technical experience


# Our assessment criteria
Here are what we look out for 

- Do you write clean code? 
- Do you consistently write unit tests?
- Do you clearly plan for tradeoffs in your solution design
- Do you use source control 

# What to do

- Use any database, data warehouse, datalake (hereby referring to these trio as “database”) of your choice based on your understanding of the problem statement
- Place your test results on a public code repository hosted on Github
- When done, share the Github repository URL to Point of Contact at Raena so he/she can review your work
- No UI is required, you can provide input using a configuration file or command line

# The Task - Get News, Store & Query Service

Create a solution that crawls for articles from any news website, sanitises the response, stores it in a database then makes it available to search via an API.

## Details

- Write an application to crawl 2 online news websites (in future, sources will grow to N where N can be 2 or more) of your choice, using a crawler framework of your choice and build the application.
- The application should sanitise the articles to obtain only specific information - Title, Author, DateTime, Lead Paragraph. You can use tools online to remove content such as advertising and html
- Store the data in a hosted database, including the original url.
- Write an API that provides access to the content.  
- User should be able to search for articles by keywords

# ++ Points
Deploy the solution as a public API using an Amazon EC2 instance
Documentation as a Knowledge Base for future work

## Design
There are 3 part of the pipeline 
1. Fetcher
2. Indexer
3. Search API

# Fetcher
we are using Scrapy Framework to crawl news website and push the news article to kafka 
Currently it is running just once and getting news we can run it as scheduled tasks periodically and get the latest news

# Indexer   
it will read from kafka and push that to elasticsearch. In between we can to transformations and call annotations api if required in future(eg: sentiment)  

# Search API
It is a wrap to query elasticsearch cluster. Currently we can query like 
``curl -XPOST "http://localhost:8081/v1/fetch" -H 'Content-Type: application/json' -d'{ "word": "Ramnath Kovind" }'`` 
and it will return news articles  
In future we can write ast query parser and pass more complex query like "Rahul Gandhi and Modi and author:Ashutosh and source:IndiaToday" 

# To Run
You will need docker-destop in mac to run it
brew install kafka
brew tap elastic/tap
brew install elastic/tap/elasticsearch-full
 
To deploy in docker just run 
docker-compose up
it will run fetcher n search_api in docker
for spark job it not easy to run in local docker beccause it will require remote repo for jar and all
so for that you can open the mvn project indexer in ide and run IndexerDriver class

Unit Test: not done yet but it will only can be done for crawler. all other process is not doing anything function it read righting to external system like elastic search or kafka to it will require system test
 