package com.assignment.indexer;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.*;
import org.apache.spark.sql.types.StructType;

import java.util.concurrent.TimeUnit;

public class IndexerDriver {
    public static void main(String[] args) throws StreamingQueryException {
        SparkSession spark = SparkSession.builder().appName("Indexer").master("local").getOrCreate();
        Dataset<Row> df = spark
                .readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", "localhost:9092")
                .option("subscribe", "news_data")
                .option("startingOffsets", "earliest")
                .load();
        StructType schema = DataTypes.createStructType(new StructField[]{
                DataTypes.createStructField("id", DataTypes.StringType, true),
                DataTypes.createStructField("title", DataTypes.StringType, true),
                DataTypes.createStructField("body", DataTypes.StringType, true),
                DataTypes.createStructField("publishDate", DataTypes.StringType, true),
                DataTypes.createStructField("author", DataTypes.StringType, true),
                DataTypes.createStructField("url", DataTypes.StringType, true)
        });
        StreamingQuery query = df.selectExpr("cast(value as String) as json_string")
                .withColumn("json_data", functions.from_json(functions.col("json_string"), schema)).
                        selectExpr("json_data.*")
                .writeStream()
                .outputMode(OutputMode.Append()) // only append mode is currently supported
                .format("es")
                .option("checkpointLocation", "/tmp/checkpointLocation")
                .trigger(Trigger.ProcessingTime(60, TimeUnit.SECONDS))
                .start("news/article");
        query.awaitTermination();

    }
}
