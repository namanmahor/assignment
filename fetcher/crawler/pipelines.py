# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from kafka import KafkaProducer
from json import dumps

class CrawlerPipeline:

    def __init__(self, kafka_servers, kafka_topic):
        self.kafka_servers=[kafka_servers]
        self.kafka_topic=kafka_topic

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            kafka_servers=crawler.settings.get('KAFKA_SERVERS'),
            kafka_topic=crawler.settings.get('KAFKA_TOPIC')
        )

    def open_spider(self, spider):
        self.producer = KafkaProducer(bootstrap_servers=self.kafka_servers,
                                      key_serializer=lambda x: x.encode('utf-8'),
                                      value_serializer=lambda x: dumps(x).encode('utf-8'))

    def process_item(self, item, spider):
        self.producer.send(self.kafka_topic, key=item['id'] ,value=dict(item))
        return item

    def close_spider(self, spider):
        self.producer.close()
