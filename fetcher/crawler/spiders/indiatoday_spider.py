import scrapy
import hashlib
from scrapy.loader import ItemLoader
from crawler.items import NewsItem


class IndiaTodaySpider(scrapy.Spider):
    name = "IndiaToday"
    start_urls = ['https://www.indiatoday.in/india/']

    def parse(self, response):
        for news_url in response.css('.detail a ::attr("href")').extract():
            if '/video/' in news_url or '/photo/' in news_url :
                continue
            yield response.follow(news_url, callback=self.parse_news,cb_kwargs={'news_url':response.urljoin(news_url)})
        next_page = response.css('.pager-next a ::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)

    def parse_news(self, response,news_url):
        il = ItemLoader(item=NewsItem())
        il.add_value("id",hashlib.md5(news_url.encode('utf-8')).hexdigest())
        il.add_value('title',' '.join(response.css(".node").xpath("./h1/text()").extract()))
        il.add_value('body',' '.join(response.css(".description").xpath(".//p/text()").extract()))
        il.add_value('publishDate', ' '.join(response.css(".profile-byline").xpath("./dt[@class='update-data']/span/text()").extract()).replace('UPDATED: ',''))
        il.add_value('author',' '.join(response.css(".profile-byline").xpath('./span/dt/a/text()').extract()))
        il.add_value('url',news_url)
        yield il.load_item()
