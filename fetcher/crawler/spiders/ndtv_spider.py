import scrapy
import hashlib
from scrapy.loader import ItemLoader
from crawler.items import NewsItem


class IndiaTodaySpider(scrapy.Spider):
    name = "NDTV"
    start_urls = ['https://www.ndtv.com/india']

    def parse(self, response):
        for news_url in response.css('.newsHdng a ::attr("href")').extract():
            if '/video/' in news_url or '/photo/' in news_url :
                continue
            yield response.follow(news_url, callback=self.parse_news,cb_kwargs={'news_url':news_url})
        next_page = response.css('.next ::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)

    def parse_news(self, response,news_url):
        il = ItemLoader(item=NewsItem())
        il.add_value("id",hashlib.md5(news_url.encode('utf-8')).hexdigest())
        il.add_value('title',' '.join(response.css(".sp-ttl").xpath("./text()").extract()))
        il.add_value('body',' '.join(response.css(".story__content").xpath("./p/text()").extract()))
        il.add_value('body',' '.join(response.css(".fullstoryCtrl_fulldetails").xpath("./p/text()").extract()))
        il.add_value('body',' '.join(response.css(".ins_storybody").xpath("./p/text()").extract()))
        il.add_value('publishDate', ' '.join(response.css(".pst-by_lnk").xpath("./span/text()").extract()).replace('Updated: ',''))
        il.add_value('author',' '.join(response.xpath('.//span[@itemprop="author"]//span/text()').extract()))
        il.add_value('url',news_url)
        yield il.load_item()
